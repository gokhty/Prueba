package com.example.myapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by goyo on 29/05/2018.
 */
public class AdapterAlumno extends BaseAdapter {
    private Activity activity;
    private ArrayList<Alumno> l;

    public AdapterAlumno(Activity c, ArrayList<Alumno> l){
        this.activity = c;
        this.l = l;
    }

    @Override
    public int getCount() {
        return l.size();
    }

    @Override
    public Object getItem(int i) {
        return null;//l.get(i)
    }

    @Override
    public long getItemId(int i) {
        return 0;//l.get(i).getCodigo() // getCodigo() debe ser tipo int
    }

    @Override
    public View getView(int posicion, View view, ViewGroup viewGroup) {

        View v = view;
        if(view == null){
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.adapter_alumno, null);
        }

        TextView lblCodigo = (TextView)v.findViewById(R.id.id_lblCodigo_alu);
        TextView lblNombre = (TextView)v.findViewById(R.id.id_lblNombre_alu);
        TextView lblSeccion = (TextView)v.findViewById(R.id.id_lblSeccion_alu);

        lblCodigo.setText(l.get(posicion).getCodigo());
        lblNombre.setText(l.get(posicion).getNombre());

        Seccion s = new Seccion();
        s.setCodigo(l.get(posicion).getSeccion().getCodigo());
        lblSeccion.setText(s.getCodigo());

        return  v;
    }
}
